// Object
/*
	- an object is a data type that is used to represent real world objects
	- it is a collection of related data and/or functionalities
	- information stored in objects are represented in "key: value" pair

	"key" - "property" of an object
	"value" - actual data to be stored in the object

	*Different data types may be stored in an object's properties
	creating complex data structures
*/

	// There are two ways in creating objects in JavaScript
		/*
			1. Object Literal Notation

			Syntax:
				let object = {}; //curly braces are called object literals
		*/

		// Object Literal Notation
		/*
			- creating objects using initializer/literal notation
			- camelCase

			Syntax:

			let objectName = {
				keyA: valueA,
				keyB: valueB
			}
		*/

		let cellphone = {
			name: "Nokia 3210",
			manufactureDate: 1999
		}
		console.log("Result from creating objects using literal notation: ");
		console.log(cellphone);

		let cellphone2 = {
			name: "OnePlus 8T",
			manufactureDate: 2020
		}
		console.log("Result from creating objects using literal notation: ");
		console.log(cellphone2);

		let ninja = {
			name: "Uzumaki Naruto",
			village: "Konoha",
			children: ["Boruto", "Himawari"]
		}
		console.log(ninja);

		/*
			2. Object Constructor Notation

			Syntax:
				let object = new Object();
		*/

		// Object Constructor Notation
		/*
			- creating objects using a constructor function
			- creates a reusable "function" to create several objects that have the same data structure

			Syntax:
				function ObjectName(keyA, keyB){
					this.keyA = keyA;
					this.keyB = keyB
				}

				let object = new ObjectName();
		*/
		// "this" keyword refers to the properties within the object
			// it allows the assignment of new object's properties
			// by associating them with values received from the constructor function's parameter (template/blueprint)
	
		function Laptop(name, manufactureDate){
			this.name = name,
			this.manufactureDate = manufactureDate;
		}

		// create an instance object using the Laptop constructor
		let laptop = new Laptop("Lenovo", 2008);
		console.log("Result from creating objects using constructor notation: ");
		console.log(laptop);

		// the "new" operator creates an instance of an object(new object)
		let myLaptop = new Laptop("Macbook Air", 2020);
		console.log("Result from creating objects using constructor notation: ");
		console.log(myLaptop);

		let laptop1 = new Laptop("Asus Zenbook Pro 16X Oled", 2022)
		console.log("Result from creating objects using constructor notation: ");
		console.log(laptop1);

		let laptop2 = new Laptop("Asus TUF Gaming F15", 2022)
		console.log(laptop2);

		let laptop3 = new Laptop("Asus ROG Zephyrus G15", 2022)
		console.log(laptop3);

		let oldLaptop = Laptop("Portal R2E CCMC", 1980);
		console.log(oldLaptop); //output: undefined

		// Creating empty objects
		let computer = {};
		let myComputer = new Object();

		console.log(computer);
		console.log(myComputer);

		// Accessing Object Properties
			// Using dot notation (RECOMMENDED, Best practice to avoid confusion with arrays)
			console.log("Result from dot notation: " + myLaptop.name);

			// Using square bracket notation (NOT recommended, might be confused with arrays)
			console.log("Result from square bracket notation: " + myLaptop["name"]);

			// Accessing array of objects
				// Accessing object properties using the square bracket notation and array indexes

				let arrayObject = [laptop, myLaptop];
				
				// may be confused with accessing array indexes
				console.log(arrayObject[0]["name"]);

				// this tells us that array[0] is an object by using the dot notation
				console.log(arrayObject[0].name);

		// Initializing / Adding / Deleting / Reassigning Object Properties
			
			//Create an object using object literals
			let car = {};
			console.log("Current value of car object: ");
			console.log(car); //output: {}

				// Initializing/Adding Object Properties (RECOMMENDED)
				car.name = "Honda Civic";
				console.log("Result from adding properties using dot notation: ");
				console.log(car); //{name: "Honda Civic"}

				// Initializing/Adding Object properties using bracket notation (NOT recommended)
				console.log("Result from adding properties using square bracket notation:");
				car["manufacture date"] = 2019
				console.log(car);

				// Deleting Object Properties using bracket notation (NOT recommended)
				delete car["manufacture date"];
				console.log("Result from deleting properties: ");
				console.log(car);

				car["manufactureDate"] = 2019;
				console.log(car);

				// Reassigning Object Property Values
				car.name = "Toyota Vios";
				console.log("Result from reassigning property values: ");
				console.log(car);

				delete car.manufactureDate; //deleted a property

		// Object Methods
			// a method is a function which is a property of an object
			// methods are useful for creating reusable functions that perform tasks related to objects

			let person = {
				name: "John",
				talk: function(){
					console.log("Hello my name is " + this.name);
				}
			}
			console.log(person);
			console.log("Result from object methods: ");
			person.talk();

			person.walk = function(steps){
				console.log(this.name + " walked " + steps + " steps forward");
			}
			console.log(person);
			person.walk(1000);

			let friend = {
				firstName: "Joe",
				lastName: "Smith",
				address: {
					city: "Austin",
					country: "Texas"
				},
				emails: ["joe@mail.com", "joesmith@mail.xyz"],
				introduce: function(){
					console.log("Hello, my name is " + this.firstName + " " + this.lastName + ". I live in " + this.address.city + ", " + this.address.country + ".");
				}
			}
			friend.introduce();

		// Real World Application of Objects
		/*
			Scenario:
			1. We would like to create a game that would have several Pokemons interact with each other
			2. Every Pokemon would have the same set of stats, properties, and function

			Stats:
				name:
				level:
				health: level*2
				attack: level
		*/

			// Create an object constructor to lessen the process in creating the pokemon
			function Pokemon(name, level){

				//properties
				this.name = name;
				this.level = level;
				this.health = level * 2;
				this.attack = level; //*1.5
				this.tackle = function(target){
					console.log(this.name + " tackled " + target.name);
				
					/*
						Mini-Activity
						- If the target health is less than or equal to 0, we will invoke the faint(method), otherwise printout the pokemon's new health
							//example, if health is not less than 0
								-rattata's health is now reduced to 5
						- reduces the target's object's health property by subtracting and reassigning it's value based on the pokemon's attack

					*/

					//target.health = target.health - this.attack;
					target.health -= this.attack; // shorthand way of writing the code in line 230

					console.log(target.name + "'s health is now reduced to " + target.health);

					if(target.health <= 0){
						target.faint();
					}
				}
				this.faint = function(target){
					console.log(target + " has fainted")
				}
			}

			let pikachu = new Pokemon ("Pikachu", 88);
			console.log(pikachu);

			let rattata = new Pokemon ("Rattata", 10);
			console.log(rattata);




