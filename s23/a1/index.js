let trainer = {
	name: "Ash Ketchum",
	age: 10,
	friends: {
		hoenn: ["May", "Max"],
		kanto: ["Brock", "Misty"]
	},
	pokemon: [
		"Pikachu",
		"Charizard",
		"Squirtle",
		"Bulbasaur"
	],
	talk: function(){
		console.log(this.pokemon[0] + ", I choose you!");
	}
}
console.log("Trainer object:");
console.log(trainer);
console.log("");

console.log("Result of dot notation:");
console.log(trainer.name);
console.log("");

console.log("Result of square bracket notation:");
console.log(trainer["pokemon"]);
console.log("");

console.log("Result of talk method:");
trainer.talk();
console.log("");


function Pokemon(name, level){
	this.name = name;
	this.level = level;
	this.health = level * 2;
	this.attack = level * 1.5;
	this.tackle = function(target){
			console.log(target.name + "'s health is " + target.health);
			console.log(this.name + " tackled " + target.name);

			target.health -= this.attack;

			console.log(this.name + "'s attack is " + this.attack);
			console.log(target.name + "'s health is now reduced to " + target.health);

			if(target.health <= 0){
				target.faint();
			}
		}
	this.faint = function(target){
		console.log(this.name + " has fainted")
	}
}

let pikachu = new Pokemon ("Pikachu", 15);
console.log(pikachu);

let geodude = new Pokemon ("Geodude", 23);
console.log(geodude);

let mewTwo = new Pokemon ("Mewtwo", 100);
console.log(mewTwo);

let lickitung = new Pokemon ("Lickitung", 10);
console.log(lickitung);
console.log("");